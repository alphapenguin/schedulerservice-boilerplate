# README #

This Solution contains a Windows service written with the TopShelf, AutoFac, Log4Net and FluentScheduler frameworks. It allows for tasks to check if the service is shutting down, and the service waits for all tasks to complete before stopping.

See this repository's [Wiki](https://bitbucket.org/alphapenguin/schedulerservice-boilerplate/wiki/) for more details.