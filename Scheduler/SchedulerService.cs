using FluentScheduler;
using log4net;
using System;
using System.Threading;

namespace Scheduler
{
    /// <summary>
    /// This is where the service is implemented. This is where your business logic starts.
    /// </summary>
    public class SchedulerService
    {
        private Registry _taskregistry;
        private ServiceShutdownNotifier _shutdownNotifier;
        private ILog _log;

        /// <summary>
        /// The Windows Service.
        /// </summary>
        /// <param name="log"></param>
        /// <param name="taskFactory"></param>
        /// <param name="taskRegistry"></param>
        /// <param name="shutdownNotifier"></param>
        public SchedulerService(ILog log, ITaskFactory taskFactory, Registry taskRegistry, ServiceShutdownNotifier shutdownNotifier)
        {
            TaskManager.TaskFactory = taskFactory;
            _taskregistry = taskRegistry;
            _shutdownNotifier = shutdownNotifier;
            _log = log;
        }

        /// <summary>
        /// Invoked when your service is asked to start. The service is considered running when this method exits.
        /// Do not take too long processing in this method, or Windows will consider your service unable to start.
        /// </summary>
        public void Start()
        {
            _log.InfoFormat("Starting");
            // Make sure unexpected task errors are logged
            TaskManager.UnobservedTaskException += TaskManager_UnobservedTaskException;
            // Allow tasks to run and start immediate tasks (using FluentScheduler)
            TaskManager.Initialize(_taskregistry);            
            _log.InfoFormat("Started");
        }

        /// <summary>
        /// This is called when there is an unhandled exception in a task. Ideally, errors should be handled inside the tasks, but this is here in case you forget.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void TaskManager_UnobservedTaskException(FluentScheduler.Model.TaskExceptionInformation sender, System.UnhandledExceptionEventArgs e)
        {
            _log.Error(string.Format("Unhandled exception in task: {0}", sender.Name), e.ExceptionObject as Exception);
        }

        /// <summary>
        /// Invoked when your service is asked to stop. The service is considered stopped when this method exits.
        /// Do not take too long processing in this method, or Windows will consider your service unable to stop.
        /// </summary>
        public void Stop()
        {
            _log.InfoFormat("Stopping");
            // Prevent more tasks from being started
            TaskManager.Stop();
            _shutdownNotifier.Stop();
            // Wait for all of them to stop
            while (TaskManager.RunningSchedules.Length > 0)
            {
                _log.InfoFormat("Waiting for all tasks to end");
                Thread.Sleep(1000);
            }
            _log.InfoFormat("Stopped");
        }
    }
}