﻿namespace Scheduler.Tasks
{
    public class TickerTaskSettings
    {
        public string TickMessage { get; set; }
        public string TockMessage { get; set; }
        public int? FatalCount { get; set; }
        public int MaxCount { get; set; }
    }
}
