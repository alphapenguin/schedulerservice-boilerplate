using FluentScheduler;
using log4net;
using System;
using System.Threading;

namespace Scheduler.Tasks
{
    /// <summary>
    /// My custom task, which runs for a while.
    /// </summary>
    public class TickerTask : ITask
    {

        private ServiceShutdownNotifier _serviceShutdownNotifier;
        private ILog _log;
        private TickerTaskSettings _settings;

        /// <summary>
        /// The notifier is injected by the Autofac container.
        /// </summary>
        /// <param name="serviceShutdownNotifier"></param>
        public TickerTask(ILog log, ServiceShutdownNotifier serviceShutdownNotifier, TickerTaskSettings settings)
        {
            _log = log;
            _serviceShutdownNotifier = serviceShutdownNotifier;
            _settings = settings;

            // Some simple validation
            if (string.IsNullOrWhiteSpace(_settings.TickMessage))
            {
                throw new ArgumentException("TickMessage must not be null or empty");
            }
            if (string.IsNullOrWhiteSpace(_settings.TockMessage))
            {
                throw new ArgumentException("TockMessage must not be null or empty");
            }
        }

        public void Execute()
        {
            for (var i = 0; i < _settings.MaxCount; i++)
            {
                // Do some "work"
                _log.Debug(i % 2 == 0 ? _settings.TickMessage : _settings.TockMessage);

                // Test what happens when an exception is thrown
                if (_settings.FatalCount != null && _settings.FatalCount > 0 && i == _settings.FatalCount) throw new Exception("Fatal error!");

                // Check if we have been asked to shut down
                if (_serviceShutdownNotifier.IsStopping)
                {
                    _log.Warn("Interrupted!");
                    break; // exit loop early
                }
                Thread.Sleep(TimeSpan.FromSeconds(1));
            }
            _log.Info("Task ended");
        }
    }
}