using FluentScheduler;

namespace Scheduler
{    
    using Tasks;

    /// <summary>
    /// My task registry.
    /// This is where all the scheduled tasks are configured.
    /// See https://github.com/fluentscheduler/FluentScheduler for details.
    /// </summary>
    public class MyTaskRegistry : Registry
    {
        public MyTaskRegistry()
        {
            // TODO Configure schedules here.
            Schedule<TickerTask>().ToRunNow().AndEvery(60).Seconds();
        }
    }
}