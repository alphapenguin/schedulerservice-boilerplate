using Autofac;
using FluentScheduler;

namespace Scheduler.AutofacUtil
{
    /// <summary>
    /// FluentScheduer Task factory for Autofac. General helper task.
    /// </summary>
    public class AutofacTaskFactory : ITaskFactory
    {
        IComponentContext _context;

        public AutofacTaskFactory(IComponentContext ctx)
        {
            _context = ctx;
        }

        public ITask GetTaskInstance<T>() where T : ITask
        {
            return _context.Resolve<T>();
        }
    }
}