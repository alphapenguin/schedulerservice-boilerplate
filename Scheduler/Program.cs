﻿namespace Scheduler
{
    using Autofac;
    using Autofac.Settings;
    using FluentScheduler;
    using log4net;
    using log4net.Config;
    using System.Reflection;
    using Topshelf;
    using Topshelf.Autofac;    

    class Program
    {
        static void Main(string[] args)
        {
            // Read log4net xml config 
            XmlConfigurator.Configure();
            var log = LogManager.GetLogger(typeof(Program));

            // Configure our Autofac Container
            log.Debug("Building Autofac container");
            var builder = new ContainerBuilder();

            // Register provider for AppSettings (Autofac.Settings)
            builder.RegisterSource(new SettingsSource());
            builder.RegisterType<SettingsFactory>().AsImplementedInterfaces();
            builder.RegisterType<AppConfigSettingsProvider>().AsImplementedInterfaces();

            // Register all AutoFac modules in the assembly:
            //builder.RegisterAssemblyModules(Assembly.GetExecutingAssembly());
            //--or just register the log4net helper module:
            // This module allows injection of ILog instances to work properly, 
            // in that each injected instance will be named by the class into which it is injected.
            builder.RegisterModule<AutofacUtil.LoggingModule>();

            // Register my Registry
            builder.RegisterType<MyTaskRegistry>().As<Registry>();

            // Register the Autofac task factory
            builder.RegisterType<AutofacUtil.AutofacTaskFactory>().As<ITaskFactory>();

            // Register all ITask implementations found in the assembly (just the executing assemly here)
            builder.RegisterAssemblyTypes(Assembly.GetExecutingAssembly())
                .Where(t => t.IsAssignableTo<ITask>())
                .AsSelf();
            //--or just register a specific Task
            //builder.RegisterType<TickerTask>();

            // Registering all the types might be easier than registering individual types
            // if you have a lot of types and/or have them in a separate assembly:
            //builder.RegisterAssemblyTypes(Assembly.GetEntryAssembly());         
            // You should ideally have at least a where-filter on this, though (see above)   

            // Register my service
            builder.RegisterType<SchedulerService>();

            // Register my service shutdown notifier helper
            // This could just be instansiated in the service itself,
            // but this is a simple example of how to register arbitrary instances in AutoFac.
            // Under normal circumstances, this would be instanciated by the service itself.
            builder.RegisterInstance(new ServiceShutdownNotifier());

            // Tell Autofac to build all the things
            var container = builder.Build();

            // Configure TopShelf
            log.Debug("Starting topShelf");
            HostFactory.Run(cfg =>
            {
                // Read service name, desription, etc. from AssemblyInfo
                cfg.UseAssemblyInfoForServiceInfo();
                // TopShelf will use AutoFac
                cfg.UseAutofacContainer(container);
                // TopShelf will use log4net
                cfg.UseLog4Net();
                // Prompt for credentials on install
                cfg.RunAsPrompt();
                // The service class
                cfg.Service<SchedulerService>(s =>
                {
                    s.ConstructUsingAutofacContainer();
                    s.WhenStarted(svc => svc.Start());
                    s.WhenStopped(svc => svc.Stop());
                });
            });

        }
    }

}
