using System;
using System.Threading;

namespace Scheduler
{
    /// <summary>
    /// Helper class to shut down tasks.
    /// </summary>
    public class ServiceShutdownNotifier : IDisposable
    {
        // This could also be implemented as a Mutex, Semaphore or something like ManualResetEvent
        private CancellationTokenSource _cts = new CancellationTokenSource();

        public void Stop() { _cts.Cancel(); }
        

        public bool IsStopping
        {
            get { return _cts.Token.IsCancellationRequested; }
        }

        #region IDisposable Support
        private bool disposedValue = false; // To detect redundant calls

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    // TODO: dispose managed state (managed objects).
                    _cts.Dispose();
                }
                
                // TODO: set large fields to null.                
                _cts = null;

                disposedValue = true;
            }
        }

        // This code added to correctly implement the disposable pattern.
        public void Dispose()
        {
            // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
            Dispose(true);
        }
        #endregion
    }
}